
Cypress.Commands.add('loginfunction', (username,password) => {
    cy.visit(Cypress.config().baseUrl)
    cy.get('[data-cy=Email]').type(username)
    cy.get('[data-cy=Password]').type(password)
    cy.get('[data-cy=SignIn]').click()
})

Cypress.Commands.add('loginToComtravo', () => {  
    cy.loginfunction("robot+qatask@comtravo.com","Qatask@08")
})

Cypress.Commands.add('AssertionForLogin', () => {
    cy.get('.initials-badge').should('be.visible')
})

Cypress.Commands.add('selectSophieInTravelers', () => {  
    cy.get('.ctr-chips__input-container > .ctr-form-field-input').type("Sophie")
    cy.get('[data-cy=Sophie]').click()
})

Cypress.Commands.add('SelectBerlinInDeparture', () => {  
    cy.get('[formcontrolname="origin"] > [cdkoverlayorigin=""] > .mb-0 > .ctr-form-field__input-container > .ctr-form-field-input').type("BER")
    cy.get('[data-cy="Berlin Brandenburg"]').click()
})

Cypress.Commands.add('SelectMunichInDestination', () => {  
    cy.get('.col-12.ng-untouched > [cdkoverlayorigin=""] > .mb-0 > .ctr-form-field__input-container > .ctr-form-field-input').type("MUC")
    cy.get('[data-cy="Franz Josef Strauss"]').click()
})

Cypress.Commands.add('SelectJourneyDates', () => {  
    cy.get('.ctr-input-datepicker').click()
        cy.get('.ctr-datepicker__next-month > ctr-calendar > .ctr-calendar > ctr-header > .ctr-header > .ctr-header__arrow--next > .ctr-header__button').click()
        cy.get('.ctr-datepicker__next-month > ctr-calendar > .ctr-calendar > ctr-header > .ctr-header > .ctr-header__arrow--next > .ctr-header__button').click()
        cy.xpath("(//button[@class='ctr-month__date-button'])[7]").click()
        cy.xpath("(//button[@class='ctr-month__date-button'])[8]").click()
})

Cypress.Commands.add('ClickonSearchFlights', () => { 
    cy.get('[data-cy="Search flights"]').click()
})

Cypress.Commands.add('AssertionForFlightsListPage', () => { 
    cy.get('.ctr-search-result-item').should('be.visible')
})

Cypress.Commands.add('SelectFlightFromList', () => { 
    cy.get(':nth-child(2) > ctr-card > .ctr-card > .ctr-card__content-container > .item-card__info-container > .item-card__info > ctr-item-card-info > .ctr-search-result-item > .ctr-search-result-item-price > [data-cy=SelectFlightOption]').click()
})

Cypress.Commands.add('AssertionForCheckOutPage', () => { 
    cy.get('.ct-font-20').should('contain.text','Flight checkout')
})

Cypress.Commands.add('AssertionForBookingConfirmation', () => { 
    cy.get('.confirmation-message__headline > .booking-checkout-message__text').should('contain.text','Thank you!')
})