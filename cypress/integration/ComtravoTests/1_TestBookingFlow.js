/// <reference types="Cypress" />

describe('Test Booking Flow', function () {

    it('TicketBooking_Testcase', function () {
    
        cy.visit(Cypress.config().baseUrl)
        cy.get('[data-cy=Email]').type("robot+qatask@comtravo.com")
        cy.get('[data-cy=Password]').type("Qatask@08")
        cy.get('[data-cy=SignIn]').click()
        
        //Assertion to check if User is logged in
        cy.get('.initials-badge').should('be.visible')

        cy.get('.ctr-chips__input-container > .ctr-form-field-input').type("Sophie")
        cy.get('[data-cy=Sophie]').click()

        cy.get('[formcontrolname="origin"] > [cdkoverlayorigin=""] > .mb-0 > .ctr-form-field__input-container > .ctr-form-field-input').type("BER")
        cy.get('[data-cy="Berlin Brandenburg"]').click()
        
        cy.get('.col-12.ng-untouched > [cdkoverlayorigin=""] > .mb-0 > .ctr-form-field__input-container > .ctr-form-field-input').type("MUC")
        cy.get('[data-cy="Franz Josef Strauss"]').click()
    
        cy.get('.ctr-input-datepicker').click()
        cy.get('.ctr-datepicker__next-month > ctr-calendar > .ctr-calendar > ctr-header > .ctr-header > .ctr-header__arrow--next > .ctr-header__button').click()
        cy.get('.ctr-datepicker__next-month > ctr-calendar > .ctr-calendar > ctr-header > .ctr-header > .ctr-header__arrow--next > .ctr-header__button').click()

        cy.xpath("(//button[@class='ctr-month__date-button'])[7]").click(0)
        cy.xpath("(//button[@class='ctr-month__date-button'])[8]").click(0)

        cy.get('[data-cy="Search flights"]').click()
        cy.get('.ctr-search-result-item').should('be.visible')
        cy.get(':nth-child(2) > ctr-card > .ctr-card > .ctr-card__content-container > .item-card__info-container > .item-card__info > ctr-item-card-info > .ctr-search-result-item > .ctr-search-result-item-price > [data-cy=SelectFlightOption]').click()
        cy.get('.ct-font-20').should('contain.text','Flight checkout')
        cy.get('.ctr-checkout-traveler__traveler-name').should('contain.text','Sophie lie')
        cy.get('.ctr-form-action__content > .button').click()
        cy.get('.confirmation-message__headline > .booking-checkout-message__text').should('contain.text','Thank you!')

    })
})