/// <reference types="Cypress" />

describe('Test Booking Flow using functions', function () {

    it('TicketBooking_Testcase', function () {
    
           
        cy.loginToComtravo()

        //Assertion to check if User is logged in
        cy.AssertionForLogin()

        cy.selectSophieInTravelers()

        cy.SelectBerlinInDeparture()
        
        cy.SelectMunichInDestination()
    
        cy.SelectJourneyDates()

        cy.ClickonSearchFlights()

        //Assertion for List of Flights page
        cy.AssertionForFlightsListPage()
        
        cy.SelectFlightFromList()
        
        //Assertion for Checkout page
        cy.AssertionForCheckOutPage()

        //Assertion to check User is Sophie lie
        cy.get('.ctr-checkout-traveler__traveler-name').should('contain.text','Sophie lie')
        cy.get('.ctr-form-action__content > .button').click()

        //Assertion for Booking confirmation page
        cy.AssertionForBookingConfirmation()

    })
})