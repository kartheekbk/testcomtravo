# TestComtravo



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

cd existing_repo
git remote add origin https://gitlab.com/kartheekbk/testcomtravo.git
git branch -M main
git push -uf origin main
```
## Executing the Tests from Local
1. The repo can be cloned/downloaded into local
2. Open the downloaded repo with VS Code
3. In VS Code, open Terminal and enter "npm install cypress"
4. After Cypress Installation, enter "npx cypress open"
5. Cypress console will be opened and Tests can be executed easily from the Console

## Executing the Tests on GitLab
1. Open the Repo in Gitlab
2. Open pipeline present under CI/CD
3. Click on Run Pipeline
4. Click on the Pipeline job triggered to watch the Console while execution
5. Once exectuion is done, Test Result will be displayed and a Video of execution is saved under artefacts
Eg: https://gitlab.com/kartheekbk/testcomtravo/-/jobs/1567177605/artifacts/browse/cypress/videos/ComtravoTests/


## About Tests in this Repo
1. There are Two Test Cases in this repo, both are same in terms of functionality.
2. Test Case 2 is same as Test Case 1 but there are more functions used which can be further reused when writing or adding more Test Cases. 
